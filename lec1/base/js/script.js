$(function() {
 // jQuery goes here...

  //Uncomment this line to fade out the red box on page load
   $(".red-box").fadeTo(2000 , 0.3);
   $(".green-box").fadeTo(2000 , 0.5);
   $(".blue-box").fadeTo(2000 , 0.8);

   $(".blue-box").fadeToggle();
   $(".blue-box").fadeToggle();
   $(".blue-box").hide();
   $(".blue-box").show();

   $(".green-box").slideUp(2000);
   $(".green-box").toggle();
   $("p").hide();
   $("p").slideDown(2000);
  
});